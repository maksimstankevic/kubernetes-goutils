package kftest

import (
	"regexp"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
)

func AwsCloudWatchGetLogEvents(sess *session.Session, logGroupName *string, logStreamName string) (*cloudwatchlogs.GetLogEventsOutput, error) {
	svc := cloudwatchlogs.New(sess)

	out, err := svc.DescribeLogStreams(&cloudwatchlogs.DescribeLogStreamsInput{
		LogGroupName: logGroupName,
	})
	if err != nil {
		return nil, err
	}

	m := regexp.MustCompile(logStreamName + `[^"]*`)

	nameString := m.FindString(out.GoString())

	logStreamNamePointer := &nameString

	resp, err := svc.GetLogEvents(&cloudwatchlogs.GetLogEventsInput{
		LogGroupName:  logGroupName,
		LogStreamName: logStreamNamePointer,
	})
	if err != nil {
		return nil, err
	}

	_, error := svc.DeleteLogGroup(&cloudwatchlogs.DeleteLogGroupInput{
		LogGroupName: logGroupName,
	})
	if error != nil {
		return resp, error
	}

	return resp, nil
}
