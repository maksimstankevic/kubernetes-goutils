package kftest

func MiscCheckExceptionPanicIfNeeded(e error) {
	if e != nil {
		panic(e)
	}
}
