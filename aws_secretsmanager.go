package kftest

import (
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

// region used for SecretsManager service creation, secret_arn is absolute aws ARN
// of the secret, key is the Secret Key as a secret can contain multiple keys-values
func AwsSecretsManagerReadSecret(region string, secret_arn string, key string) string {

	sess, err := session.NewSession()
	MiscCheckExceptionPanicIfNeeded(err)

	svc := secretsmanager.New(
		sess,
		aws.NewConfig().WithRegion(region),
	)

	getSecretInput := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(secret_arn),
	}

	getSecretOutput, err := svc.GetSecretValue(getSecretInput)
	MiscCheckExceptionPanicIfNeeded(err)

	nice_secrets_string := string([]byte(*getSecretOutput.SecretString)[1 : len([]byte(*getSecretOutput.SecretString))-1])
	secret_strings_slice := strings.Split(nice_secrets_string, ",")

	var token string = ""

	for _, string := range secret_strings_slice {
		if strings.Contains(string, key) {
			token = (strings.Split(string, ":"))[1]
			token = token[1 : len(token)-1]
		}
	}

	return token
}
