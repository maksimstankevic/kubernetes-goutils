package kftest

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"path/filepath"
	"time"

	corev1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/remotecommand"
)

func K8sGetK8sClient(kube_config string) (*kubernetes.Clientset, *restclient.Config) {

	kubeconfig := flag.String("kubeconfig", filepath.Join("..", "example", kube_config), "(optional) absolute path to the kubeconfig file")
	flag.Parse()

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	MiscCheckExceptionPanicIfNeeded(err)

	clientset, err := kubernetes.NewForConfig(config)
	MiscCheckExceptionPanicIfNeeded(err)

	return clientset, config
}

func K8sCreateNginxPod(podName string, namespace string, clientset *kubernetes.Clientset) (string, error) {

	var ip string = ""
	var w watch.Interface

	pod := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:   podName,
			Labels: map[string]string{"team": "dev"},
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:            "webserver",
					Image:           "nginx",
					ImagePullPolicy: "IfNotPresent",
					Ports: []corev1.ContainerPort{{
						ContainerPort: 80,
					}},
				},
			},
			RestartPolicy: "Never",
		},
	}

	resp, err := clientset.CoreV1().Pods(namespace).Create(context.Background(), pod, metav1.CreateOptions{})
	if err != nil {
		return ip, err
	}

	log.Printf("Pod created: %s", resp)

	if w, err = clientset.CoreV1().Pods(namespace).Watch(context.Background(), metav1.ListOptions{
		Watch:           true,
		ResourceVersion: resp.ResourceVersion,
		FieldSelector:   fields.Set{"metadata.name": podName}.AsSelector().String(),
		LabelSelector:   labels.Everything().String(),
	}); err != nil {
		return ip, err
	}

	func() {
		for {
			select {
			case events, ok := <-w.ResultChan():
				if !ok {
					return
				}
				resp = events.Object.(*corev1.Pod)
				log.Println("Pod status:", resp.Status.Phase)
				if resp.Status.Phase != corev1.PodPending {
					w.Stop()
					return
				}
			case <-time.After(10 * time.Second):
				log.Println("timeout to wait for pod active")
				w.Stop()
				return
			}
		}
	}()
	if resp.Status.Phase != corev1.PodRunning {
		return ip, fmt.Errorf("Pod is unavailable: %v", resp.Status.Phase)
	}

	pods, err := clientset.CoreV1().Pods(namespace).List(context.Background(), metav1.ListOptions{})
	MiscCheckExceptionPanicIfNeeded(err)

	for _, pod := range pods.Items {
		if pod.Name == podName {
			ip = pod.Status.PodIP
		}
	}

	return ip, nil
}

func K8sCreateNamespace(namespace string, labels map[string]string, clientset *kubernetes.Clientset) error {
	nsName := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:   namespace,
			Labels: labels,
		},
	}

	_, err := clientset.CoreV1().Namespaces().Create(context.Background(), nsName, metav1.CreateOptions{})
	if err != nil {
		return err
	}
	return nil
}

func K8sCreateNetworkPolicy(namespace string, policy *netv1.NetworkPolicy, clientset *kubernetes.Clientset) error {

	_, err := clientset.NetworkingV1().NetworkPolicies(namespace).Create(context.Background(), policy, metav1.CreateOptions{})
	if err != nil {
		return err
	}

	return nil
}

func K8sExecInPod(client kubernetes.Interface, config *restclient.Config, podName string, namespace string,
	command string, stdout io.Writer) error {

	var stdin io.Reader = nil
	var stderr io.Writer = &bytes.Buffer{}

	cmd := []string{
		"sh",
		"-c",
		command,
	}
	req := client.CoreV1().RESTClient().Post().Resource("pods").Name(podName).
		Namespace(namespace).SubResource("exec")
	option := &corev1.PodExecOptions{
		Command: cmd,
		Stdin:   false,
		Stdout:  true,
		Stderr:  true,
		TTY:     true,
	}
	if stdin == nil {
		option.Stdin = false
	}

	req.VersionedParams(
		option,
		scheme.ParameterCodec,
	)
	exec, err := remotecommand.NewSPDYExecutor(config, "POST", req.URL())
	if err != nil {
		return err
	}
	err = exec.Stream(remotecommand.StreamOptions{
		Stdin:  stdin,
		Stdout: stdout,
		Stderr: stderr,
	})
	if err != nil {
		return err
	}

	return nil
}
