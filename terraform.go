package kftest

import (
	"fmt"
	"io/ioutil"
	"log"
)

func TerraformReadTerraformConfiguration(root_directory string, files []string) []byte {
	var filecontent []byte

	for _, file := range files {
		data, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", root_directory, file))
		if err != nil {
			log.Print(err)
		}
		filecontent = append(filecontent, data...)
	}
	return filecontent
}
