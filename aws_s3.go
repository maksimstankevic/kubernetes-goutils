package kftest

import (
	"strings"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/stretchr/testify/assert"
)

func AwsS3VerifyFileIsInBucket(t *testing.T, bucketName, bucketRegion, fileName string) {

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(bucketRegion)},
	)

	svcS3 := s3.New(sess)

	items, err := svcS3.ListObjectsV2(&s3.ListObjectsV2Input{Bucket: aws.String(bucketName)})
	MiscCheckExceptionPanicIfNeeded(err)

	fileFound := false

	for _, item := range items.Contents {
		if strings.Contains(*item.Key, fileName) {
			fileFound = true
		}
	}

	assert.Equal(t, fileFound, true, "No "+fileName+" found in artifact s3 bucket")
}
