package kftest

import (
	"errors"
	"log"
	"strings"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/codebuild"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/stretchr/testify/assert"
)

func AwsCodebuildIsBuildCompleted(svc *codebuild.CodeBuild, projectName *string) (bool, string) {
	builds, err := svc.BatchGetBuilds(&codebuild.BatchGetBuildsInput{Ids: []*string{projectName}})
	if err != nil {
		log.Println("Got error getting builds: ", err)
		panic(err)
	}
	if *builds.Builds[0].BuildComplete {
		return true, aws.StringValue(builds.Builds[0].BuildStatus)
	}
	return false, aws.StringValue(builds.Builds[0].BuildStatus)
}

func AwsCodebuildWaitForBuildToComplete(svc *codebuild.CodeBuild, projectName *string, maxRetry int, delay int) (bool, string) {
	var isCompleted bool = false
	var BuildStatus string = ""
	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered from AwsCodebuildIsBuildCompleted", r)
			panic("retry is giving up")
		}
	}()
	for i := 0; i < maxRetry; i++ {
		log.Println("Attempt ", i+1)
		isCompleted, BuildStatus = AwsCodebuildIsBuildCompleted(svc, projectName)
		if isCompleted == true {
			return isCompleted, BuildStatus
		}
		log.Println("Codebuild job still running. Next attempt in ", delay, "s")
		time.Sleep(time.Duration(delay) * time.Second)
	}
	log.Println("Codebuild finished.")
	return isCompleted, BuildStatus
}

func AwsCodebuildGetCBProjectnameFromBuildRunName(projectName *string) string {
	if projectName == nil {
		return ""
	}
	return strings.Split(*projectName, ":")[0]
}

func AwsCodebuildFindCBBuildRunPosition(projectNames []*string, projectName string) int {
	for index, p := range projectNames {
		pp := AwsCodebuildGetCBProjectnameFromBuildRunName(p)
		if pp == projectName {
			return index
		}
	}
	return -1
}

func AwsCodebuildGetTheBuildWithTimeout(t *testing.T, svc *codebuild.CodeBuild, projectName string, timeoutSec int) (*string, error) {

	var index int = -1
	var build_name *string = nil
	var exc error = nil

	for i := 0; i < timeoutSec; i++ {
		names, err := svc.ListBuilds(&codebuild.ListBuildsInput{SortOrder: aws.String("ASCENDING")})
		if err != nil {
			log.Println("Got error listing builds: ", err)
			panic("ListBuilds failed")
		}

		log.Println("Build names existing currently:")
		for in := range names.Ids {
			log.Println(*names.Ids[in])
		}

		index = AwsCodebuildFindCBBuildRunPosition(names.Ids, projectName)

		if index == -1 {
			log.Println("Build not triggered yet...trying again in one second...")
		} else {
			build_name = names.Ids[index]
			break
		}

		time.Sleep(1 * time.Second)
		continue

	}

	if index == -1 {
		exc = errors.New("timed out waiting for the build to start")
	}

	return build_name, exc
}

func AwsCodebuildVerifyFinalCodebuildArtifactIsOnS3(t *testing.T, svc *codebuild.CodeBuild, svcS3 *s3.S3, projectName string, timeout int, bucketName string) {

	defer func() {
		if r := recover(); r != nil {
			if r == "AwsCodebuildWaitForBuildToComplete is giving up" {
				log.Println("Recovered from AwsCodebuildWaitForBuildToComplete", r)
			} else if r == "ListBuilds failed" {
				log.Println("Recovered from ListBuilds", r)
			} else {
				log.Println("Unexpected ERROR", r)
			}
			log.Println("Giving 100 seconds for codebuild to complete before destroying resources")
			time.Sleep(100 * time.Second)
			t.Fail()
			t.Logf("Marking this test failed as not all the tests were completed")
		}
	}()

	build_name, err := AwsCodebuildGetTheBuildWithTimeout(t, svc, projectName, timeout)
	if err != nil {
		panic(err)
	}

	// FIXME: Pass AwsCodebuildIsBuildPhaseComplete function instead of the parameters of the function
	// We need apply some golang decorator pattern here
	isCodeBuildJobFinished, projectStatus := AwsCodebuildWaitForBuildToComplete(svc, build_name, 15, 10)

	assert.NotEqual(t, projectStatus, "", "Problem in AwsCodebuildIsBuildCompleted, see logs.")
	assert.NotEqual(t, isCodeBuildJobFinished, false, "The project timed out.")
	assert.NotEqual(t, projectStatus, "FAILED")
	assert.Equal(t, projectStatus, "SUCCEEDED")

	items, err := svcS3.ListObjectsV2(&s3.ListObjectsV2Input{Bucket: aws.String(bucketName)})
	if err != nil {
		log.Println("Unable to list items in bucket")
		panic(err)
	}

	buildOutputDirFound := false

	for _, item := range items.Contents {
		if strings.Contains(*item.Key, "build_outp") {
			buildOutputDirFound = true
		}
	}

	assert.Equal(t, buildOutputDirFound, true, "No build_outp folder found in artifact s3 bucket")

	log.Println("Test finished")
}

func AwsCodebuildWaitForBuildOnlyProjectNameRegionAndTimeout(t *testing.T, projectName string, projectRegion string, timeout int) {
	defer func() {
		if r := recover(); r != nil {
			if r == "AwsCodebuildWaitForBuildToComplete is giving up" {
				log.Println("Recovered from AwsCodebuildWaitForBuildToComplete", r)
			} else if r == "ListBuilds failed" {
				log.Println("Recovered from ListBuilds", r)
			} else {
				log.Println("Unexpected ERROR", r)
			}
			log.Println("Giving 100 seconds for codebuild to complete before destroying resources")
			time.Sleep(100 * time.Second)
			t.Fail()
			t.Logf("Marking this test failed as not all the tests were completed")
		}
	}()

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(projectRegion)},
	)
	svc := codebuild.New(sess)

	build_name, err := AwsCodebuildGetTheBuildWithTimeout(t, svc, projectName, timeout)
	MiscCheckExceptionPanicIfNeeded(err)

	isCodeBuildJobFinished, projectStatus := AwsCodebuildWaitForBuildToComplete(svc, build_name, 120, 10)

	assert.NotEqual(t, projectStatus, "", "Problem in AwsCodebuildIsBuildCompleted, see logs.")
	assert.NotEqual(t, isCodeBuildJobFinished, false, "The project timed out.")
	assert.NotEqual(t, projectStatus, "FAILED")
	assert.Equal(t, projectStatus, "SUCCEEDED")

}
