package kftest

import (
	"context"
	"fmt"
	"strings"

	"github.com/google/go-github/github"
)

func GithubPushCommitToExistingRepoBranch(owner string, repo string, branch string, ctx context.Context, client *github.Client) {

	// safeguard to not attempt to commit to any non-testing branches
	if branch[0:8] != "testing/" {
		panic("Please DO NOT try to commit to any branches that do not comply to pattern testing/*")
	}

	dummyFileBytes := []byte("Dummy file content.")

	fileOpts := &github.RepositoryContentFileOptions{
		Message:   github.String("terratest pushing dummy file"),
		Content:   dummyFileBytes,
		Branch:    github.String(branch),
		Committer: &github.CommitAuthor{Name: github.String("Tester at Nordcloud"), Email: github.String("tester@nordcloud.com")},
	}

	getOpts := &github.RepositoryContentGetOptions{
		Ref: branch,
	}

	fileData, _, _, err := client.Repositories.GetContents(ctx, owner, repo, "github-webhook-dummy-file.md", getOpts)
	if err != nil {
		if strings.Contains(err.Error(), "404 Not Found") {
			fmt.Println("Dummy file does not exist - pushing it")
			_, _, err := client.Repositories.CreateFile(ctx, owner, repo, "github-webhook-dummy-file.md", fileOpts)
			MiscCheckExceptionPanicIfNeeded(err)
			return
		} else {
			MiscCheckExceptionPanicIfNeeded(err)
		}
	}

	tempSHA := *fileData.SHA
	fileOpts.SHA = &tempSHA

	tempCommitMessage := *github.String("terratest deleting dummy file")
	fileOpts.Message = &tempCommitMessage

	fmt.Println("Dummy file found on the repo - deleting it")
	_, _, err = client.Repositories.DeleteFile(ctx, owner, repo, "github-webhook-dummy-file.md", fileOpts)
	if err != nil {
		fmt.Println(err)
		return
	}

}
